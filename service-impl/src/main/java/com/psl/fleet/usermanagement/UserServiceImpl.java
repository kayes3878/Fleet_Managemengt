package com.psl.fleet.usermanagement;

import com.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Picolight on 11/15/2016.
 */
@Transactional
@Service("UserService")
public class UserServiceImpl implements UserService{

    static final Logger logger = Logger.getLogger(UserServiceImpl.class.getName());


    UserRepository repository;

    @Autowired
    UserServiceImpl(UserRepository uRepository)
    {
        this.repository = uRepository;
    }




    @Override
    public boolean isUserAuthintacate(User user) {

        logger.info("Going to check the login info");

        if(user == null){
            logger.debug(String.format("user not exist",user));
            return false;
        }
        if(user.getUserName() == null || user.getUserName().length() == 0) return false;
        if(user.getPassword()== null || user.getPassword().length() == 0) return false;
        User data = repository.getUserByUserName(user.getUserName());

        int haspassword = user.getPassword().hashCode() ;
        user.setPassword(Integer.toString(haspassword));

        if(data == null) return false;

        return data.getPassword().equals(user.getPassword());
    }
    @Override
    public boolean isUserSave(User user) {

        logger.info("Going to check is user exist");

        if(user == null) return false;
//        if(user.getUserName() == null || user.getUserName().length() == 0) return false;
//        if(user.getPassword() == null || user.getPassword().length() == 0) return false;
        User data = repository.saveUser(user);
        if(data == null) return false;
        return true;
    }

    @Transactional
    @Override
    public List<User> getallUsers() {

        return repository.getallUser();
    }

    @Override
    public String welcome() {
        if(logger.isDebugEnabled()){
            logger.debug("getWelcome Message is executed!");
        }
        String msg = " welcome spring world ";
        return msg;
    }





}
