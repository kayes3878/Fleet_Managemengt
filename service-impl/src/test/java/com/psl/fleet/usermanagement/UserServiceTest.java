package com.psl.fleet.usermanagement;

import com.model.User;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Picolight on 11/19/2016.
 */
public class UserServiceTest{

    Mockery context = new Mockery();

    @Test
    public void successfullLogin()    {
        final UserRepository repository = context.mock(UserRepository.class);

        final User output = new User();
        output.setUserName("kayes");
        output.setPassword("-1408706944");

        UserService service = new UserServiceImpl(repository);

        context.checking(new Expectations() {{
            one (repository).getUserByUserName("kayes"); will(returnValue(output));
        }});

        User input = new User();
        input.setUserName("kayes");
        input.setPassword("asd123");

        assertEquals(true, service.isUserAuthintacate(input));

        context.assertIsSatisfied();

    }


    @Test
    public void shouldFailIfPasswordMismatch()    {
        final UserRepository repository = context.mock(UserRepository.class);

        final User output = new User();
        output.setUserName("kayes");
        output.setPassword("asd12");

        UserService service = new UserServiceImpl(repository);

        context.checking(new Expectations() {{
            one (repository).getUserByUserName("kayes"); will(returnValue(output));
        }});

        User input = new User();
        input.setUserName("kayes");
        input.setPassword("asd123");

        assertEquals(false, service.isUserAuthintacate(input));

        context.assertIsSatisfied();

    }

    @Test
    public void shouldwelcome()    {
        final UserRepository repository = context.mock(UserRepository.class);
        String msg = " welcome spring world ";

        UserService service = new UserServiceImpl(repository);
        assertEquals(msg, service.welcome());

        context.assertIsSatisfied();

    }

}
