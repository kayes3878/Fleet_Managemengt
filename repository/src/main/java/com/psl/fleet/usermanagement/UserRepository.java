package com.psl.fleet.usermanagement;

import com.model.User;

import java.util.List;

/**
 * Created by Picolight on 11/16/2016.
 */
public interface UserRepository {
    User getUserByUserName(String userName);
    User saveuser();
//  User PasswordEncoderGenerator (String password);
    User saveUser(User user);
    List<User> getallUser();
}
