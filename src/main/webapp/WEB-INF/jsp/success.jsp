
<%--
  Created by IntelliJ IDEA.
  User: Picolight
  Date: 11/15/2016
  Time: 3:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>${loginBean.getUserName()}</title>
</head>
<body>
<div>

        <h1>User List</h1>
        <table width="70%" cellpadding="2">
            <tr bgcolor="#5f9ea0">
                <th align="left">Id</th>
                <th align="left">Name</th>
                <th align="left">Edit</th>
                <th align="left">Delete</th>
            </tr>
            <c:forEach var="user" items="${users}">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.userName}</td>
                    <td><a href="editemp/${user.id}">Edit</a></td>
                    <td><a href="deleteuser/${user.id}">Delete</a></td>
                </tr>
            </c:forEach>
        </table>
</div>
<form action="/user" method="post">
    <input type="submit" name="newuser" value="Create New User" />
</form>
</body>
</html>
