<%--
  Created by IntelliJ IDEA.
  User: Picolight
  Date: 11/23/2016
  Time: 4:05 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
             pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Fleet Management Login Form</title>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
    <style>
        <%@ include file="/resources/theme1/css/signup.css" %>

    </style>


</head>
<body>
<h1 class="register-title">Welcome</h1>
<div align="center"><h1>${msg}</h1></div>
<form:form name="submitForm" action="/saveuser" method="POST">
    <div class="testbox">
        <table>
            <tr>
                <td> <label id="icon" for="name"> <i class="icon-user"></i> </label> </td>
                <td>
                    <input type="text" name="userName" class="register-input" id="name" placeholder="Name"/></td>
            </tr>
            <tr>
                <td> <label id="icon" for="name"><i class="icon-shield"></i></label> </td>
                <td>
                    <input type="text" name="password" class="register-input" placeholder="Password"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Create Account" class="register-button" /></td>

        </table>

        <p>By clicking Register, you agree on our <a href="#">terms and condition</a>.</p>

        <div style="color: red">${error}</div>
    </div>
</form:form>
</body>
</html>