<%--
  Created by IntelliJ IDEA.
  User: Picolight
  Date: 11/15/2016
  Time: 3:32 AM
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <style>
            <%@ include file="/resources/theme1/css/main.css" %>
            <%@ include file="/resources/theme1/js/main.js" %>
        </style>

        <link href="<c:url value="/resources/theme1/css/main.css"/>" rel="stylesheet" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Fleet Management Login Form</title>
    </head>
<body>
<div class="login-page">
<div class="msg" align="center">${msg}</div>
    <div class="form">
<form:form name="submitForm"  method="POST">
    <div align="center">
        <table>
            <tr>
                <td>User Name</td>
                <td><input type="text" placeholder="name" name="userName" /></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" placeholder="password" name="password" /></td>
            </tr>
            <tr>
                <td></td>
                <td><button type="submit" value="Submit" >Submit</button></td>
            </tr>
        </table>
        <p class="message">Not registered? <a href="<c:url value="createuser"/>">Create an account</a></p>
        <div style="color: red">${error}</div>
    </div>
</form:form>
    </div>
</div>
</body>
</html>