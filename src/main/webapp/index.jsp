<%--
  Created by IntelliJ IDEA.
  User: Picolight
  Date: 11/15/2016
  Time: 12:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <style>
    body {
      background-color: black;
    }

    #earth {
      width: 500px;
      height: 500px;
      /*background: url(http://www.noirextreme.com/digital/Earth-Color4096.jpg);*/
      border-radius: 50%;
      background-size: 210px;
      box-shadow: inset 16px 0 40px 6px rgb(0, 0, 0),
      inset -3px 0 6px 2px rgba(255, 255, 255, 0.2);
      animation-name: rotate;
      animation-duration: 4s;
      animation-iteration-count: infinite;
      animation-timing-function: linear;
    }

    @keyframes rotate {
      from { background-position-x: 0px; }
      to { background-position-x: 250px; }
    }

  </style>
</head>
<body>
<div id="earth"></div>

</body>
</html>