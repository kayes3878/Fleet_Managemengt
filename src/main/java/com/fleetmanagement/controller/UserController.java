package com.fleetmanagement.controller;

import com.model.User;
import com.psl.fleet.usermanagement.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by Picolight on 11/24/2016.
 */
public class UserController {
    UserService userService;// = new UserServiceImpl();

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/allUser", method = RequestMethod.GET)
    public List<User> getallUsers(User user) {
        return userService.getallUsers();
    }

    @RequestMapping(value = "/saveuser2" , method = RequestMethod.GET)
    public String allUser(Model model, @ModelAttribute("user") User user) {

        model.addAttribute("users", getallUsers(user));
        return "success";

    }

}
