package com.fleetmanagement.controller;

import com.model.User;
import com.psl.fleet.usermanagement.UserService;
import org.apache.log4j.Logger;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * Created by Picolight on 11/15/2016.
 */
@Controller
@Cache(type = CacheType.FULL)
public class LoginController {

    static final Logger logger = Logger.getLogger(LoginController.class.getName());

    UserService userService;// = new UserServiceImpl();

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String init(Model model) {
        logger.info("Going to create login Obj");
        model.addAttribute("msg", "Please Enter Your Login Details");
        return "login";
    }

    @RequestMapping(value = "/createuser", method = RequestMethod.GET)
    public String createUser(Model model) {

        logger.info("Going to create login Obj");
        model.addAttribute("msg", "Please Enter Your Login Details");
        return "userEntry";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String submit(Model model, @ModelAttribute("loginBean") User user, HttpServletRequest request) {
        logger.info("going to create the user");

        if(userService.isUserAuthintacate(user)) {
            model.addAttribute("msg", "welcome " + user.getUserName() + userService.welcome());
            List<User> listOfuser = userService.getallUsers();
            model.addAttribute("users", listOfuser);
            if(logger.isDebugEnabled()){
                logger.debug("getWelcome Message is executed!");
            }
            request.getSession().setAttribute("LOGGEDIN_USER", user);
            return "success";
        } else {
            model.addAttribute("error", "Please enter Details");
            logger.error("This is Error message", new Exception("Testing"));
            return "login";
        }
    }

    @Transactional
    @RequestMapping(value = "/saveuser" , method = RequestMethod.POST)
    public String addUser(Model model,@ModelAttribute("user") User user) {


        if(userService.isUserSave(user)) {
            if(logger.isDebugEnabled()){
                logger.debug("getWelcome Message is executed!");
            }
            List<User> listofuserss = userService.getallUsers();
            model.addAttribute("users", listofuserss);
            return "success";
        } else {
            List<User> listofuserss = userService.getallUsers();
            model.addAttribute("users", listofuserss);
            model.addAttribute("error", "Please enter Details");
            return "success";
        }

    }
    @RequestMapping(value = "deleteuser/{id}" , method = RequestMethod.POST)
    public String deleteUser(Model model,@ModelAttribute("user") User user) {
//        userService.deleteUser(user.getId());
        return "success";


    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String user_entry_ui(Model model, User user , HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("newuser") != null) {
            List<User> list_user = userService.getallUsers();
            model.addAttribute("users", list_user);
            return "userEntry";
        }
        else {
            List<User> list_user = userService.getallUsers();
            model.addAttribute("users", list_user);
            return "success";
        }
    }

}
