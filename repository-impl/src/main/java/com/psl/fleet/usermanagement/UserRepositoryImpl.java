package com.psl.fleet.usermanagement;

import com.model.User;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Picolight on 11/16/2016.
 */

@Transactional
@Repository("UserRepository")
@Cache(type = CacheType.FULL)
public class UserRepositoryImpl implements UserRepository{


    static final Logger logger = Logger.getLogger(UserRepositoryImpl.class.getName());



        /* Create EntityManagerFactory */
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        /* Create EntityManager */
//        EntityManager em = emf.createEntityManager();

    public User getUserByUserName(String userName)
    {
        logger.info("Going to connect the database");
        //        Connection EntityManagerFactory
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
        EntityManager entitymanager = emfactory.createEntityManager( );
        entitymanager.getTransaction( ).begin( );

        TypedQuery<com.psl.fleet.usermanagement.entity.User> q = entitymanager.createQuery("SELECT u FROM User u WHERE u.name = :name", com.psl.fleet.usermanagement.entity.User.class);
        q.setParameter("name", userName);

        com.psl.fleet.usermanagement.entity.User found = q.getSingleResult();
        if(found == null) return null;
            User result = new User();
            result.setUserName(found.getName());
            result.setPassword(found.getPassword());
        logger.info("Successfully got it");
        return result;

    }
    @Transactional
    @Override
    public User saveuser() {
        return null;
    }




//    @Override
//    public User PasswordEncoderGenerator(String password) {
////        String hashedPassword = null;
////        int i = 0;
////        while (i < 10) {
////            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
////             hashedPassword = passwordEncoder.encode(password);
////            i++;
////        }
//        if(password == null) return null;
//        User result = new User();
////        result.setPassword(hashedPassword);
//        return result;
//    }

    @Transactional
    @Override
    public User saveUser(User user) {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
        EntityManager entitymanager = emfactory.createEntityManager( );
        entitymanager.getTransaction( ).begin( );

        logger.info("Transaction begin");

        com.psl.fleet.usermanagement.entity.User userobj = new com.psl.fleet.usermanagement.entity.User( );


        String input =user.getPassword();
        int hashCode = input.hashCode();

        userobj.setName(user.getUserName());
        userobj.setPassword(Integer.toString(hashCode));



        logger.debug(String.format("saving user",userobj));
        entitymanager.persist(userobj);
        logger.info("going to persist");
        entitymanager.getTransaction( ).commit( );


//        entitymanager.flush();
        logger.debug("close the entitymanager");
        entitymanager.close( );
        emfactory.close( );

        User result = new User();
        result.setUserName(user.getUserName());
        result.setPassword(user.getPassword());



        return result;
    }
    @Transactional
    @Override
    public List<User> getallUser() {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
        EntityManager entitymanager = emfactory.createEntityManager( );
        entitymanager.getTransaction( ).begin( );

        List<com.psl.fleet.usermanagement.entity.User> userlist =   entitymanager.createNamedQuery("User.finedall", com.psl.fleet.usermanagement.entity.User.class).getResultList();
        List<User> users_list = new ArrayList<User>();
        for (com.psl.fleet.usermanagement.entity.User userobjlist : userlist)
        {
            User userobj = new User();
            userobj.setId(userobjlist.getId());
            userobj.setUserName(userobjlist.getName());
            userobj.setPassword(userobjlist.getPassword());
            users_list.add(userobj);
        }
        return users_list;
    }

}
