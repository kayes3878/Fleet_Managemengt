package com.psl.fleet.usermanagement;

import com.model.User;

import java.util.List;

/**
 * Created by Picolight on 11/15/2016.
 */
public interface UserService {

    public boolean isUserAuthintacate(User user) ;
    public String welcome() ;
    public boolean isUserSave(User user);
    List<User> getallUsers();
//    public UserService(User user.getId);
}
